from django.urls import path
from receipts.views import (
    receipts, create_receipt,
    categories, create_category,
    accounts, create_account)


urlpatterns = [
    path("receipts/", receipts, name="home"),
    path("receipts/create/", create_receipt, name="create_receipt"),
    path("receipts/categories/", categories, name="category_list"),
    path("receipts/accounts/", accounts, name="account_list"),
    path("receipts/categories/create/", create_category, name="create_category"),
    path("receipts/accounts/create/", create_account, name="create_account"),
]
