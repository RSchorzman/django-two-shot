from django.urls import path
from accounts.views import (
    user_login, user_logout,
    signup)


urlpatterns = [
    path("accounts/login/", user_login, name="login"),
    path("accounts/logout/", user_logout, name="logout"),
    path("accounts/signup/", signup, name="signup"),
]
